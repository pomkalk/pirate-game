import * as PIXI from 'pixi.js'
import * as io from 'socket.io-client'

var app = PIXI.autoDetectRenderer()
document.body.appendChild(app.view)
app.view.style.position = 'absolute'
app.view.style.display = 'block'
app.resize(window.innerWidth, window.innerHeight)
var gmap
var loadingText = new PIXI.Text('Loading...', {
  fill: 0xffffff,
  fontSize: 24
})
loadingText.anchor.set(0.5)
loadingText.position.set(window.innerWidth / 2, window.innerHeight / 2)
var loadingScene = new PIXI.Container()
var bg = new PIXI.Container()
loadingScene.addChild(loadingText)
app.render(loadingScene)
var textures = {}
var scale = 1

var socket = io(GAME_SERVER)
socket.on('map', (map) => {
  gmap = map
  var uniqnum = [].concat.apply([], map.layers)
  var inum = uniqnum.filter((item, index) => {
    if (item === 0) return false
    return uniqnum.indexOf(item) == index
  })

  var loader = new PIXI.loaders.Loader()

  inum.forEach(n => {
    loader.add(n.toString(), GAME_SERVER + '/images/tiles/tile_' + (n < 10 ? '0' + n : n) + '.png')
  })
  //loader.add('ship', 'kenney_piratepack/PNG/Default size/Ships/ship (1).png')
  loader.load(() => {
    inum.forEach(n => {
      textures[n] = loader.resources[n].texture
    })

    map.layers.forEach(layer => {
      var lc = new PIXI.Container()
      for (var i = 0; i < 50; i++) {
        for (var j = 0; j < 50; j++) {
          var index = 50 * j + i
          if (layer[index] !== 0) {
            var sprite = new PIXI.Sprite(textures[layer[index]])
            sprite.position.set(i * 64, j * 64)
            lc.addChild(sprite)
          }
        }
      }
      bg.addChild(lc)
    })

    loop()

    socket.emit('map_loaded')
  })
})

function loop() {
  requestAnimationFrame(loop)
  bg.scale.set(scale)
  bg.position.x = window.innerWidth / 2 - gmap.width * gmap.tileSize / 2 * scale
  bg.position.y = window.innerHeight / 2 - gmap.height * gmap.tileSize / 2 * scale
  if (scale < 0.3) scale = 0.3
  app.render(bg)
}