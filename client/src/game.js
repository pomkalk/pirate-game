import * as PIXI from 'pixi.js'
import * as io from 'socket.io-client'
import * as Stats from 'stats.js'

class Game {
  constructor() {
    PIXI.utils.skipHello()
    this.app = PIXI.autoDetectRenderer()
    this.app.view.style.position = 'absolute'
    this.app.view.style.display = 'block'
    document.body.appendChild(this.app.view)
    this.stats = new Stats()
    document.body.appendChild(this.stats.dom)
    this.stats.showPanel(0)
    this.resize()
    this.state = null
    this.states = {'loading': this.showLoading}
    window.onresize = this.resize.bind(this)
    this.draw = this.showLoading
    this.socket = io(GAME_SERVER)
    this.socket.on('connect', this.connected.bind(this))
    this.map = null
    this.users = {}
    this.textures = {}
    this.pid = 0
    this.scene = new PIXI.Container();
    this.bg = new PIXI.Container()
    this.fg = new PIXI.Container();
    requestAnimationFrame(this.update.bind(this))
  }

  resize () {
    this.width = window.innerWidth
    this.height = window.innerHeight
    this.center = {
      x: this.width * 0.5,
      y: this.height * 0.5
    }
    this.app.resize(this.width, this.height)
  }

  connected () {
    this.socket.on('disconnect', () => {
      this.fg.removeChildren()
      this.draw = this.showLoading
      this.users = {}
    })
    this.socket.on('map', (map) => {
      this.map = map
      var allTiles = [].concat.apply([this.map.background], this.map.layers)
      var uniqTiles = allTiles.filter((item, index) => {
        if (item === 0) return false
        return allTiles.indexOf(item) == index
      })
      this.loader = new PIXI.loaders.Loader()
      uniqTiles.forEach(n => {
        this.loader.add(n.toString(), GAME_SERVER + '/images/tiles/tile_' + (n < 10 ? '0' + n : n) + '.png')
      })
      this.loader.add('ship3', GAME_SERVER + '/images/ship/3.png')
      this.loader.add('ship2', GAME_SERVER + '/images/ship/2.png')
      this.loader.add('ship1', GAME_SERVER + '/images/ship/1.png')
      this.loader.add('ship0', GAME_SERVER + '/images/ship/0.png')
      this.loader.load(() => {
        uniqTiles.forEach(n => {
          this.textures[n] = this.loader.resources[n].texture
        })
        var bg = new PIXI.Container()
        for (var i = - map.width; i < map.width * 2; i++) {
          for (var j = -map.height; j < map.height * 2; j++) {
            var sprite = new PIXI.Sprite(this.textures[map.background])
            sprite.position.set(i * map.tileSize, j * map.tileSize)
            bg.addChild(sprite)
          }
        }
        this.bg.addChild(bg)
        this.map.layers.forEach(layer => {
          var lc = new PIXI.Container()
          for (var i = 0; i < map.width; i++) {
            for (var j = 0; j < map.height; j++) {
              var index = map.height * j + i
              if (layer[index] !== 0) {
                var sprite = new PIXI.Sprite(this.textures[layer[index]])
                sprite.position.set(i * map.tileSize, j * map.tileSize)
                lc.addChild(sprite)
              }
            }
          }
          this.bg.addChild(lc)
        })
        this.socket.emit('map_loaded')
        this.draw = this.preview
      })
    })

    this.socket.on('users', (users) => {
      users.forEach(user => {
        if (this.users.hasOwnProperty(user.id)) {
        } else {
          this.users[user.id] = {
            name: user.name,
            hp: user.hp,
            sprite: new PIXI.Sprite(this.loader.resources['ship' + user.hp].texture)
          }
          this.users[user.id].sprite.position = user.p
          this.users[user.id].sprite.anchor.set(0.5)
          this.fg.addChild(this.users[user.id].sprite)
        }
      })

      // DEBUG
      setTimeout(()=>{
        this.socket.emit('play', {name: 'user#'+this.socket.id}, (player_id) => {
          this.player_id = player_id
          this.bg.interactive = true
          this.bg.on('pointerup', () => {
            this.socket.emit('next_player', (player_id) => {
              this.player_id = player_id
            })
          })
          this.draw = this.playing
        })
      }, 100)
    })

    this.socket.on('add_user', (user) => {
      this.users[user.id] = {
        name: user.name,
        hp: user.hp,
        sprite: new PIXI.Sprite(this.loader.resources['ship' + user.hp].texture)
      }
      this.users[user.id].sprite.position = user.p
      this.users[user.id].sprite.anchor.set(0.5)
      this.fg.addChild(this.users[user.id].sprite)
    })

    this.socket.on('remove_user', (user_id) => {
      if (this.users.hasOwnProperty(user_id)) {
        this.users[user_id].sprite.destroy()
        delete this.users[user_id]
      }
    })

    this.socket.on('update', (res) => {
      if (res.pid >= this.pid) {
        this.pid = res.pid
        res.data.forEach(user => {
          if (this.users.hasOwnProperty(user.id)) {
            this.users[user.id].sprite.position = user.p
            this.users[user.id].sprite.rotation = user.a
          }
        })
      }
    })
  }

  showLoading () {
    var x = (new Date()).getSeconds() % 3 + 1
    var text = new PIXI.Text('Loading' + '.'.repeat(x), { fill: 0xffffff})
    text.anchor.set(0.5)
    text.position.set(this.center.x, this.center.y)
    var scene = new PIXI.Container()
    scene.addChild(text)
    this.app.render(scene)
  }

  preview () {
    this.bg.position.x = window.innerWidth / 2 - this.map.width * this.map.tileSize / 2 
    this.bg.position.y = window.innerHeight / 2 - this.map.height * this.map.tileSize / 2 
    this.fg.position.x = window.innerWidth / 2 - this.map.width * this.map.tileSize / 2
    this.fg.position.y = window.innerHeight / 2 - this.map.height * this.map.tileSize / 2
    this.app.render(this.bg)
  }

  playing () {
    var player = this.users[this.player_id]
    var f = 0.8
    this.bg.position.x = this.bg.position.x + ((-player.sprite.position.x + this.width / 2) - this.bg.position.x) * f
    this.bg.position.y = this.bg.position.y + ((-player.sprite.position.y + this.height / 2) - this.bg.position.y) * f
    this.fg.position.x = this.bg.position.x + ((-player.sprite.position.x + this.width / 2) - this.bg.position.x) * f
    this.fg.position.y = this.bg.position.y + ((-player.sprite.position.y + this.height / 2) - this.bg.position.y) * f
    this.scene.removeChildren()
    this.scene.addChild(this.bg)
    this.scene.addChild(this.fg)
    this.app.render(this.scene)
  }

  update () {
    requestAnimationFrame(this.update.bind(this))
    this.stats.begin()
    this.draw()
    this.stats.end()
  }
} 

export default Game
