class Game {
  constructor() {
    this.t0 = 0
    this.t1 = 0
  }
  _update() {
    requestAnimationFrame(this._update.bind(this))
    this.t0 = performance.now()
    this.update(this.delta)
    this.t1 = performance.now()
    this.delta = this.t1 - this.t0
  }
  start() {
    requestAnimationFrame(this._update.bind(this))
  }
  update(delta) {

  }
}

export default Game