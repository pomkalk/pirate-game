var path = require('path')
var webpack = require('webpack')

var dev = {
  'GAME_SERVER': '"http://localhost:3000"'
}

var prod = {
  'GAME_SERVER': '""'
}

module.exports = env => {
  return {
    mode: env.dev ? 'development' : 'production',
    entry: './src/app.js',
    output: {
      path: path.resolve(__dirname, 'build'),
      publicPath: '/build/',
      filename: 'app.js'
    },
    plugins: [
      new webpack.DefinePlugin(env.dev ? dev : prod)
    ]
  }
}