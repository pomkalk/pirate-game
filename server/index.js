var express = require('express')
var app = express()
var cors = require('cors')
var http = require('http').Server(app)
var io = require('socket.io')(http)
var { Engine, World, Bodies, Body, Bounds, Events, Common } = require('matter-js')
var { loadMap, prints, printe, getRandomPoint } = require('./src/utils')
var models = require('./src/models')
// matter js required
global.document = {
  createElement: function () {
    // Canvas
    return {
      getContext: function () {
        return {};
      }
    };
  }
};
global.window = {};

app.use(cors())
app.use(express.static('./static'))

var users = {}
var ui = 0
var pid = 0
// Start loading server
prints('Loading map...')
loadMap('default', (err, map) => {
  if (err) {
    return console.log(err)
  }
  printe('ok')
  prints('Creating map...')
  // Create Physics Engine
  var engine = Engine.create()
  // Loading Map to ENGINE
  for (i = 0; i < map.width; i++) {
    for (j = 0; j < map.height; j++) {
      var ind = map.height * j + i
      var n = map.static[ind]
      if (n !== 0) {
        if (models.hasOwnProperty(n)) {
          var body = Bodies.fromVertices(0, 0, models[n].data, {isStatic: true})
          World.add(engine.world, body)
          Body.setPosition(body, {
            x: i * map.tileSize - body.bounds.min.x + body.position.x + models[n].offset.x,
            y: j * map.tileSize - body.bounds.min.y + body.position.y + models[n].offset.y
          })
        }
      }
    }
  }
  //Creating game border
  World.add(engine.world, [
    Bodies.rectangle(map.borderOffset, map.height * map.tileSize / 2, map.borderWidth, map.height * map.tileSize, {isStatic: true}),
    Bodies.rectangle(map.width * map.tileSize - map.borderOffset, map.height * map.tileSize / 2, map.borderWidth, map.height * map.tileSize, {isStatic: true}),
    Bodies.rectangle(map.width * map.tileSize / 2, map.borderOffset, map.width * map.tileSize, map.borderWidth, {isStatic: true}),
    Bodies.rectangle(map.width * map.tileSize / 2, map.height * map.tileSize - map.borderOffset, map.width * map.tileSize, map.borderWidth, {isStatic: true})
  ])
  printe('ok')
  prints('Starting server...')

  io.on('connect', (socket) => {
    console.log('User connected', socket.id)
    socket.on('disconnect', () => {
      console.log('User disconnected', socket.id)
      if (socket.user_id) {
        World.remove(engine.world, users[socket.user_id].body)
        io.emit('remove_user', socket.user_id)
        socket.user_id = null
      }
    })

    socket.emit('map', map)
    socket.on('map_loaded', () => {
      console.log('User load map', socket.id)
      socket.join('players')
      socket.emit('users', users_list())
    })

    socket.on('play', (name, cb) => {
      console.log('player', name, 'started game')
      var ship_position = getRandomPoint(map)
      var id = '#' + Common.nextId()
      var name = name
      var body = Bodies.fromVertices(ship_position.x, ship_position.y, models['ship'])
      users[id] = { id, name, body, hp: 3 }
      World.add(engine.world, body)
      io.emit('add_user', {
        id,
        name: users[id].name,
        p: users[id].body.position,
        a: users[id].body.angle,
        hp: users[id].hp
      })
      socket.user_id = id
      console.log('User created:', id)
      cb(Object.keys(users)[ui++])
    })

    socket.on('next_player', (cb) => {
      cb(Object.keys(users)[ui++])
    })
  })

  function users_list() {
    return Object.keys(users).map((user_id) => {
      return {
        id: user_id,
        name: users[user_id].name,
        p: users[user_id].body.position,
        a: users[user_id].body.angle,
        hp: users[user_id].hp
      }
    })
  }

  function users_update() {
    return {
      pid: pid++,
      data: Object.keys(users).map(user_id => {
        return { id: user_id, p: users[user_id].body.position, a: users[user_id].body.angle, hp: users[user_id].hp};
      })
  };
  }

  function create_bot () {
    var ship_position = getRandomPoint(map)
    var id = '#' + Common.nextId()
    var name = 'User ' + id
    var body = Bodies.fromVertices(ship_position.x, ship_position.y, models['ship'])
    users[id] = { id, name, body, bot: true, hp: 3 }
    World.add(engine.world, body)
    console.log('Bot created:', id)
  }

  http.listen(3000, () => {
    printe('ok')
    engine.world.gravity.y = 0
    Engine.run(engine)

    // for( i=0;i< 50; i++ ) {
    //   create_bot()
    // }

    // Events.on(engine, 'afterTick', (e) => {
    //   Object.keys(users).forEach(user_id => {
    //     var user = users[user_id]
    //     if (users[user_id].bot) {
    //     	var speed = 5
    //       var vel = {
    //         x: - speed * Math.sin(users[user_id].body.angle),
    //         y: speed * Math.cos(users[user_id].body.angle)
    //       }
    //       Body.setVelocity(users[user_id].body, vel)
    //     }
    //   })
    // })
    
    setInterval(() => {
      io.to('players').emit('update', users_update())
    }, 1000/60)
    // Events.on(engine, 'afterUpdate', (e) => {
    //   io.to('players').emit('update', users_update())
    // })
  })
})