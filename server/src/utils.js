var fs = require('fs')
var path = require('path')
var { Common } = require('matter-js')
module.exports = {
  loadMap (map, cb) {
    fs.exists(path.resolve('maps', map + '.json'), (exists) =>{
      if (exists) {
        fs.readFile(path.resolve('maps', map + '.json'), (err, data) => {
          if (err) return cb(err)
          try {
            var json = JSON.parse(data)
            return cb(null, json)
          } catch (e) {
            cb(e)
          }
        })
      } else {
        cb(new Error('Map ' + map + ' not founded.'))
      }
    })
  },
  prints (text) {
    process.stdout.write(text)
  },
  printe (text) {
    console.log(text)
  },
  getRandomPoint(map) {
    var x, y
    while (true) {
      x = Math.floor(Math.random() * (map.width * map.tileSize - map.tileSize * 2)) + map.tileSize
      y = Math.floor(Math.random() * (map.height * map.tileSize - map.tileSize * 2)) + map.tileSize
      var xi = Math.floor(x / map.tileSize)
      var yi = Math.floor(y / map.tileSize)
      if (map.static[map.height * yi + xi] === 0 ) break
    }
    return {x, y}
  }
}