module.exports = {
    offset: {
      x: 0,
      y: 0
    },
    data: [
      {x: 0, y: 62},
      {x: 30, y: 54},
      {x: 48, y: 45},
      {x: 55, y: 39},
      {x: 60, y: 28},
      {x: 62, y: 24},
      {x: 62, y: 0},
      {x: 0, y: 0}]
}