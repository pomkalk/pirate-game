module.exports = {
    offset: {
      x: 1,
      y: 0
    },
    data: [
      {x: 64, y: 0},
      {x: 2, y: 0},
      {x: 3, y: 16},
      {x: 2, y: 34},
      {x: 1, y: 48},
      {x: 2, y: 64},
      {x: 64, y: 64}]
}